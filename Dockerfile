FROM alpine:3.10

ARG VSFTPD_VERSION=3.0.3-r6
RUN apk add --update --no-cache \
        tzdata \
        vsftpd=$VSFTPD_VERSION

RUN adduser -h /home/./files -s /bin/false -D files

ENV TZ=UTC

RUN echo "local_enable=YES" >> /etc/vsftpd/vsftpd.conf && \
  echo "chroot_local_user=YES" >> /etc/vsftpd/vsftpd.conf && \
  echo "allow_writeable_chroot=YES" >> /etc/vsftpd/vsftpd.conf && \
  echo "write_enable=YES" >> /etc/vsftpd/vsftpd.conf &&\
  echo "local_umask=022" >> /etc/vsftpd/vsftpd.conf &&\
  echo "passwd_chroot_enable=yes" >> /etc/vsftpd/vsftpd.conf &&\
  echo 'seccomp_sandbox=NO' >> /etc/vsftpd/vsftpd.conf &&\
  echo 'pasv_enable=Yes' >> /etc/vsftpd/vsftpd.conf &&\
  echo 'use_localtime=Yes' >> /etc/vsftpd/vsftpd.conf && \
  echo 'pasv_max_port=10100' >> /etc/vsftpd/vsftpd.conf &&\
  echo 'pasv_min_port=10090' >> /etc/vsftpd/vsftpd.conf &&\
  sed -i "s/anonymous_enable=YES/anonymous_enable=YES/" /etc/vsftpd/vsftpd.conf

VOLUME /home/files

EXPOSE 20 21 10090-10100

COPY --chown=1001:0 entrypoint.sh /usr/local/bin/

CMD /usr/local/bin/entrypoint.sh
