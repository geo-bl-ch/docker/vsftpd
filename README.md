Simple alpine based image to offer a single user vsftpd (aka very safe ftp deamon).

User:

`files`

Volume:

`/home/files`

Password:

`random or passed as ENV "PASSWORD"`

How to use it:

```
$ docker run -it --rm  -v <path to files you want to host>:/home/files registry.gitlab.com/geo-bl-ch/docker/vsftpd:latest
Generated password for user 'files': fF3X25cg
Password for 'files' changed

$ docker run -it --rm  -v <path to files you want to host>:/home/files -e PASSWORD=files registry.gitlab.com/geo-bl-ch/docker/vsftpd:latest
Password for 'files' changed
```